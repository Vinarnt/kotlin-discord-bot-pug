package com.omega.module.pug.database

import com.omega.discord.bot.database.DAO
import com.omega.module.pug.service.pug.`object`.Match
import sx.blah.discord.handle.obj.IGuild
import sx.blah.discord.handle.obj.IUser


interface PUGMatchDAO : DAO<Match> {

    fun initiatedMatchesCount(guild: IGuild): Long

    fun startedMatchesCount(guild: IGuild): Long

    fun stoppedMatchesCount(guild: IGuild): Long

    fun findPlayedBy(guild: IGuild, user: IUser): List<Match>
}