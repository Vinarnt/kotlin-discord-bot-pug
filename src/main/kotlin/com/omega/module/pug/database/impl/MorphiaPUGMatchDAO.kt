package com.omega.module.pug.database.impl

import com.omega.module.pug.database.PUGMatchDAO
import com.omega.module.pug.service.pug.`object`.Match
import org.bson.types.ObjectId
import org.mongodb.morphia.Datastore
import sx.blah.discord.handle.obj.IGuild
import sx.blah.discord.handle.obj.IUser


class MorphiaPUGMatchDAO(private val datastore: Datastore) : PUGMatchDAO {

    override fun find(id: ObjectId): Match? =
        datastore.get(Match::class.java, id)


    override fun insert(entity: Match): Match {
        datastore.save(entity)

        return entity
    }

    override fun update(entity: Match): Match {
        datastore.save(entity)

        return entity
    }

    override fun delete(entity: Match) {
        datastore.delete(entity)
    }

    override fun initiatedMatchesCount(guild: IGuild): Long =
        datastore.createQuery(Match::class.java)
            .field("guild").equal(guild)
            .count()

    override fun startedMatchesCount(guild: IGuild): Long =
        datastore.createQuery(Match::class.java)
            .field("guild").equal(guild)
            .field("startedAt").notEqual(null)
            .count()

    override fun stoppedMatchesCount(guild: IGuild): Long =
        datastore.createQuery(Match::class.java)
            .field("guild").equal(guild)
            .field("stoppedAt").notEqual(null)
            .count()

    override fun findPlayedBy(guild: IGuild, user: IUser): List<Match> {
        datastore.createQuery(Match::class.java).let {
            it.and(
                it.criteria("guild").equal(guild),
                it.or(
                    it.criteria("teamOne.players.user").equal(user),
                    it.criteria("teamTwo.players.user").equal(user)
                )
            )
            return it.asList()
        }
    }

    override fun clean() {
    }
}