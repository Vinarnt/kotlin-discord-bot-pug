package com.omega.module.pug

import com.omega.discord.bot.command.CommandRegistry
import com.omega.discord.bot.database.DatabaseManager
import com.omega.discord.bot.permission.PermissionRegistry
import com.omega.module.pug.command.impl.*
import com.omega.module.pug.database.impl.MorphiaPUGMatchDAO
import com.omega.module.pug.permission.Permission
import com.omega.module.pug.service.pug.`object`.Match
import sx.blah.discord.api.IDiscordClient
import sx.blah.discord.modules.IModule


class PUGModule : IModule {

    private val commands = arrayOf(
        PUGStartCommand(),
        PugStopCommand(),
        PUGInfoCommand(),
        PUGAddCommand(),
        PUGRemoveCommand(),
        PUGPickCommand(),
        PUGStatsCommand(),
        PUGLogCommand()
    )

    override fun enable(client: IDiscordClient): Boolean {
        DatabaseManager.apply {
            addDAO(MorphiaPUGMatchDAO::class)
            mapClasses(
                Match::class.java
            )
        }
        commands.forEach { CommandRegistry.register(it) }
        Permission.values().forEach { PermissionRegistry.register(it) }

        return true
    }

    override fun getName(): String = "PUG"

    override fun getVersion(): String = "0.1.0"

    override fun getMinimumDiscord4JVersion(): String = "2.10.1"

    override fun getAuthor(): String = "Kyu"

    override fun disable() {
        DatabaseManager.removeDAO(MorphiaPUGMatchDAO::class)
        commands.forEach { CommandRegistry.unregister(it) }
        Permission.values().forEach { PermissionRegistry.unregister(it) }
    }
}