package com.omega.module.pug.service.pug.`object`


data class Slot(val position: SlotPosition, var player: Player? = null)