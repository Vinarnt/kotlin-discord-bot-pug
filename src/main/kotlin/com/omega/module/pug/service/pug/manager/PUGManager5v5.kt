package com.omega.module.pug.service.pug.manager

import com.omega.module.pug.service.pug.PUGManagerListener
import com.omega.module.pug.service.pug.`object`.Slot
import com.omega.module.pug.service.pug.`object`.SlotPosition


class PUGManager5v5(listener: PUGManagerListener) : PUGManager(listener) {

    override val maxKeepers: Int = 2
    override val maxDefenders: Int = 2
    override val maxMidfielders: Int = 6
    override val type: Type = Type.T5V5

    init {
        arrayOf(match.teamOne, match.teamTwo).forEach {
            with(it.slots) {
                add(Slot(SlotPosition.KEEPER))
                add(Slot(SlotPosition.DEFENDER))
                add(Slot(SlotPosition.MIDFIELDER))
                add(Slot(SlotPosition.MIDFIELDER))
                add(Slot(SlotPosition.MIDFIELDER))
            }
        }
    }

    override fun isPositionAllowed(position: SlotPosition): Boolean = true
}