package com.omega.module.pug.service.pug

import com.omega.discord.bot.database.DatabaseManager
import com.omega.module.pug.database.PUGMatchDAO
import com.omega.module.pug.service.pug.`object`.Match
import sx.blah.discord.handle.obj.IGuild
import sx.blah.discord.handle.obj.IUser


object PUGMatchLogger {

    private val matchDAO: PUGMatchDAO by lazy { DatabaseManager.getDAO(PUGMatchDAO::class)!! }

    fun addMatch(match: Match) {
        matchDAO.insert(match)
    }

    fun updateMatch(match: Match) {
        matchDAO.update(match)
    }

    fun initiatedMatchesCount(guild: IGuild): Long =
        matchDAO.initiatedMatchesCount(guild)

    fun startedMatchesCount(guild: IGuild): Long =
        matchDAO.startedMatchesCount(guild)

    fun stoppedMatchesCount(guild: IGuild): Long =
        matchDAO.stoppedMatchesCount(guild)

    fun getMatchesPlayedBy(guild: IGuild, user: IUser): List<Match> =
        matchDAO.findPlayedBy(guild, user)
}