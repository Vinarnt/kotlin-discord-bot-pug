package com.omega.module.pug.service.pug

import com.omega.discord.bot.service.MessageSender
import com.omega.module.pug.service.pug.`object`.PUGException
import com.omega.module.pug.service.pug.`object`.Player
import com.omega.module.pug.service.pug.`object`.SlotPosition
import com.omega.module.pug.service.pug.manager.*
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.IUser


object PUGService {

    private const val NO_MANAGER_ERROR = "You need to set a pug mode with command _pugstart_ (Permission needed)"

    private val managers: MutableMap<IChannel, PUGManager> = mutableMapOf()

    fun start(startedBy: IUser, channel: IChannel, type: PUGManager.Type) {
        // Check if a match is already started
        managers[channel]
            ?.let {
                // Check if the match is playing
                if (it.phase == PUGManager.Phase.PLAY)
                    null
                else
                    MessageSender.sendMessage(channel, "**A match is already started**")
            }
            ?: when (type) { // If no manager found, create one
                PUGManager.Type.T2v2 -> PUGManager2v2(PUGManagerListener(channel))
                PUGManager.Type.T3V3 -> PUGManager3v3(PUGManagerListener(channel))
                PUGManager.Type.T5V5 -> PUGManager5v5(PUGManagerListener(channel))
                PUGManager.Type.T6V6 -> PUGManager6v6(PUGManagerListener(channel))
            }.also {
                // Add manager to list and start it
                managers[channel] = it
                it.start(channel, startedBy)
            }
    }

    fun stop(stoppedBy: IUser, channel: IChannel) {
        managers[channel]
            ?.let {
                it.stop(stoppedBy)
                managers.remove(channel)
            }
            ?: throw PUGException("Can't stop PUG match, none was running")
    }

    fun printInfo(channel: IChannel): String =
        managers[channel]?.printInfo() ?: throw PUGException(NO_MANAGER_ERROR)

    /**
     * Add a user to the manager queue
     * @param channel in which channel
     * @param user the user to add
     * @throws PUGException if position queue is full
     */
    fun add(channel: IChannel, user: IUser, position: SlotPosition) {
        managers[channel]?.add(user, position) ?: throw PUGException(NO_MANAGER_ERROR)
    }

    /**
     * Remove the user from the manager queue
     * @param channel in which channel
     * @param user the user to remove
     * @return the removed player or null
     */
    fun remove(channel: IChannel, user: IUser): Player? =
        (managers[channel] ?: throw PUGException(NO_MANAGER_ERROR)).remove(user)

    /**
     * Pick a player from queue to put in picking user team
     * @param channel
     * @param pickingUser
     * @param pickedUser
     */
    fun pick(channel: IChannel, pickingUser: IUser, pickedUser: IUser) {
        managers[channel]?.pick(pickingUser, pickedUser) ?: throw PUGException(NO_MANAGER_ERROR)
    }

    fun printRegistration(channel: IChannel): String =
        managers[channel]?.printRegistrationPhase() ?: throw PUGException(NO_MANAGER_ERROR)

    fun printPick(channel: IChannel): String =
        managers[channel]?.printPickPhase() ?: throw PUGException(NO_MANAGER_ERROR)
}