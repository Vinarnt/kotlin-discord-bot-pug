package com.omega.module.pug.service.pug.manager

import sx.blah.discord.handle.obj.IGuild


object PUGStatsManager {

//    private val matchDAO: PUGMatchDAO = DatabaseManager.getDAO(PUGMatchDAO::class)!!

    private val guildManagers: MutableMap<IGuild, GuildStatManager> = mutableMapOf()


    private fun getGuildStatsManager(guild: IGuild): GuildStatManager =
        guildManagers[guild] ?: GuildStatManager(
            guild
        ).also {
            guildManagers[guild] = it
        }

    class GuildStatManager(val guild: IGuild) {

    }
}