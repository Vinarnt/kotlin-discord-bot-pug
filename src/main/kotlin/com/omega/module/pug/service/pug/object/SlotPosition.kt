package com.omega.module.pug.service.pug.`object`


enum class SlotPosition(val title: String) {
    KEEPER("Keeper"),
    DEFENDER("Defender"),
    MIDFIELDER("Midfielder")
}