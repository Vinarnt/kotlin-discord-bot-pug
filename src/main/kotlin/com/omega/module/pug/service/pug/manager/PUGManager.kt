package com.omega.module.pug.service.pug.manager

import com.omega.discord.bot.BotManager
import com.omega.module.pug.service.pug.PUGManagerListener
import com.omega.module.pug.service.pug.PUGMatchLogger
import com.omega.module.pug.service.pug.`object`.Match
import com.omega.module.pug.service.pug.`object`.Player
import com.omega.module.pug.service.pug.`object`.SlotPosition
import sx.blah.discord.api.events.EventSubscriber
import sx.blah.discord.handle.impl.events.user.PresenceUpdateEvent
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.IUser
import sx.blah.discord.handle.obj.StatusType
import java.time.LocalDateTime

abstract class PUGManager(private val listener: PUGManagerListener) {

    enum class Type(val title: String) {
        T2v2("2 v 2"),
        T3V3("3 v 3"),
        T5V5("5 v 5"),
        T6V6("6 v 6")
    }

    enum class Phase(val title: String) {
        REGISTRATION("Registration"),
        PICKING("Picking"),
        PLAY("Play")
    }

    protected val userPlayerMapper: MutableMap<IUser, Player> = mutableMapOf()
    protected val keeperRegistrationQueue: MutableList<Player> = mutableListOf()
    protected val defenderRegistrationQueue: MutableList<Player> = mutableListOf()
    protected val midfielderRegistrationQueue: MutableList<Player> = mutableListOf()
    protected val match = Match()
    var phase: Phase = Phase.REGISTRATION
        get
        protected set
    protected var pickingPlayer: Player? = null

    abstract val maxKeepers: Int
    abstract val maxDefenders: Int
    abstract val maxMidfielders: Int
    abstract val type: Type

    /**
     * Start the manager
     */
    fun start(channel: IChannel, startedBy: IUser) {
        BotManager.client.dispatcher.registerListener(this)
        this.match.apply {
            this.channel = channel
            this.guild = channel.guild
            this.type = this@PUGManager.type
            this.initiatedBy = startedBy
            this.initiatedAt = LocalDateTime.now()
        }
        listener.onStart(this)
        PUGMatchLogger.addMatch(match)
    }

    /**
     * Stop the manager
     */
    fun stop(stoppedBy: IUser) {
        BotManager.client.dispatcher.unregisterListener(this)
        this.match.apply {
            this.stoppedBy = stoppedBy
            this.stoppedAt = LocalDateTime.now()
        }
        listener.onStop(this)
        PUGMatchLogger.updateMatch(match)
    }

    /**
     * Print info on the current phase
     */
    fun printInfo() =
        when (phase) {
            Phase.REGISTRATION -> printRegistrationPhase()
            Phase.PICKING -> printPickPhase()
            Phase.PLAY -> printPlayPhase()
        }

    /**
     * Add a user to queue
     * @param user the user to add
     * @param position at which position to put him
     */
    fun add(user: IUser, position: SlotPosition) {
        // Check if we are in registration phase
        if (phase != Phase.REGISTRATION)
            listener.onError("Can't add in ${phase.title} phase")
        // Check that position is allowed
        else if (!isPositionAllowed(position))
            listener.onError("Position $position not allowed in ${type.title}")
        else {
            when (position) {
                SlotPosition.KEEPER ->
                    Triple(keeperRegistrationQueue, maxKeepers, position)

                SlotPosition.DEFENDER ->
                    Triple(defenderRegistrationQueue, maxDefenders, position)

                SlotPosition.MIDFIELDER ->
                    Triple(midfielderRegistrationQueue, maxMidfielders, position)

            }.let { (registrationQueue, max, position) ->
                if (registrationQueue.size < max) {
                    // Remove user from last slot
                    remove(user, true)

                    // Create player and add it to queue
                    Player(user, position).also {
                        userPlayerMapper[user] = it
                        match.registrationQueue.add(it)
                        registrationQueue.add(it)

                    }
                } else listener.onError("No ${position.title} slot available").let { null }
            }?.also { player ->
                // Check if registration queue is full
                if (getRegisteredUserCount() == getMaxRegisteredUserCount()) {
                    // Go to next phase (picking)
                    this.phase = Phase.PICKING
                    keeperRegistrationQueue.apply { shuffle() }.let {
                        this.pickingPlayer = it.first()
                        this.match.teamOne.setKeeper(it[0])
                        this.match.teamTwo.setKeeper(it[1])
                    }

                    listener.onRegistrationComplete(this, player)
                } else {
                    listener.onRegistrationAdd(this, player)
                }

                // Update persisted match
                PUGMatchLogger.updateMatch(match)
            }
        }
    }

    /**
     * Remove the user from registration queue
     * @param user the user to remove from queue
     * @param programmatically if the remove is done by the user or from the program
     * @return the removed player or null
     */
    fun remove(user: IUser, programmatically: Boolean = false): Player? {
        // Check if we are in registration phase
        if (phase != Phase.REGISTRATION)
            listener.onError("Can't remove in ${phase.title} phase")
        else {
            // Check that user is registered
            userPlayerMapper[user]
                ?.let { player ->
                    userPlayerMapper.remove(user)
                    match.registrationQueue.remove(player)
                    when (player.position) {
                        SlotPosition.KEEPER -> keeperRegistrationQueue.remove(player)
                        SlotPosition.DEFENDER -> defenderRegistrationQueue.remove(player)
                        SlotPosition.MIDFIELDER -> midfielderRegistrationQueue.remove(player)
                        else -> false
                    }.also { removed ->
                        if (!programmatically && removed) {
                            listener.onRegistrationRemove(this, player)
                        }
                    }

                    return player
                }
                ?: if (!programmatically) listener.onError("No need to remove you, ${user.mention()}, you were not in queue")
        }

        return null
    }

    abstract fun isPositionAllowed(position: SlotPosition): Boolean

    /**
     * Print the registration state
     */
    fun printRegistrationPhase(): String =
        buildString {
            appendln("```css")
            appendln(printMatchTitle(Phase.REGISTRATION))
            append("  - Keepers (${keeperRegistrationQueue.size}/$maxKeepers): ")
            appendln("[${keeperRegistrationQueue.joinToString { player -> player.user!!.getDisplayName(match.channel?.guild) }}]")
            if (maxDefenders > 0) {
                append("  - Defenders (${defenderRegistrationQueue.size}/$maxDefenders): ")
                appendln("[${defenderRegistrationQueue.joinToString { player -> player.user!!.getDisplayName(match.channel?.guild) }}]")
            }
            append("  - Midfielders (${midfielderRegistrationQueue.size}/$maxMidfielders): ")
            appendln("[${midfielderRegistrationQueue.joinToString { player -> player.user!!.getDisplayName(match.channel?.guild) }}]")
            append("```")
        }

    /**
     * Pick a player for a position on the team
     */
    fun pick(pickingUser: IUser, pickedUser: IUser) {
        // Check that we are in picking phase
        if (phase != Phase.PICKING)
            listener.onError("Can't pick in ${phase.title} phase")
        else {
            val pickingPlayer = userPlayerMapper[pickingUser]
            val pickedPlayer = userPlayerMapper[pickedUser]

            // Check that it's the user turn
            if (pickingPlayer == null || this.pickingPlayer != pickingPlayer)
                listener.onError("You can't pick, it's ${this.pickingPlayer?.user?.getDisplayName(match.channel?.guild)} turn")

            // Check that the picked player is in the match
            else if (pickedPlayer == null)
                listener.onError("The picked player is not in the match")

            // Check that the picking player have a free slot for the picked player
            else if (!checkFreeSlot(pickingPlayer, pickedPlayer))
                listener.onError("You can't pick anymore for the ${pickedPlayer.position!!.title} position")

            // Check that the picked player is not already in a team
            else if (pickedPlayer.team != null)
                listener.onError("The picked player already have been picked")

            // Everything is ok, pick
            else {
                val pickerTeam = pickingPlayer.team!!
                when (pickedPlayer.position) {
                    SlotPosition.KEEPER -> throw IllegalStateException("At this state, we shouldn't be here")
                    SlotPosition.DEFENDER -> {
                        pickerTeam.setDefender(pickedPlayer)

                        // We can set the other player in the opposite team
                        (if (pickerTeam == match.teamOne) match.teamTwo else match.teamOne)
                            .let { team ->
                                defenderRegistrationQueue.find { player -> player.team == null }
                                    ?.let { team.setDefender(it) }
                            }
                    }

                    SlotPosition.MIDFIELDER -> {
                        pickerTeam.addMidfielder(pickedPlayer)

                        // We can add the last midfielder to the other team
                        if (midfielderRegistrationQueue.count { it.team == null } == 1) {
                            (if (pickerTeam == match.teamOne) match.teamTwo else match.teamOne)
                                .let { team ->
                                    midfielderRegistrationQueue.find { player -> player.team == null }
                                        ?.let { team.addMidfielder(it) }
                                }
                        }
                    }
                }

                // Check if picking phase is complete
                if (getPickedUserCount() == getMaxRegisteredUserCount()) {
                    this.phase = Phase.PLAY
                    listener.onPickComplete(this, pickingPlayer, pickedPlayer)

                    this.match.startedAt = LocalDateTime.now()
                } else { // Continue to pick
                    this.pickingPlayer =
                        if (pickingPlayer.team == match.teamOne) match.teamTwo.getKeeper() else match.teamOne.getKeeper()
                    listener.onPick(this, pickingPlayer, pickedPlayer, this.pickingPlayer!!)
                }

                PUGMatchLogger.updateMatch(match)
            }
        }
    }

    /**
     * Print the picking turn notice
     */
    fun printPickingTurnNotice() =
        buildString {
            append("It's your turn to pick ")
            append(pickingPlayer?.user?.mention())
            append(", use **__!pugp <userMention>__** to pick a player")
        }

    /**
     * Print the picking state
     */
    fun printPickPhase(complete: Boolean = false): String =
        buildString {
            if (complete)
                appendln("**Picking phase is now complete.**")

            appendln(printPickBody(if (complete) Phase.PLAY else Phase.PICKING))

            if (complete) {
                append("**__Get ready please: __**")
                append(match.registrationQueue.joinToString { it.user!!.mention() })
            }
        }

    fun printPickablePlayers(): String =
        match.registrationQueue
            .filter { player -> player.team == null }
            .let { pickablePlayers ->
                buildString {
                    appendln("```css")
                    appendln("Available players:")
                    appendln()
                    if (maxDefenders > 0) {
                        val defenders = pickablePlayers.filter { player -> player.position == SlotPosition.DEFENDER }
                        append("  - Defenders: ")
                        appendln("[${defenders.joinToString { player -> player.user!!.getDisplayName(match.channel?.guild) }}]")
                    }
                    val midfielders = pickablePlayers.filter { player -> player.position == SlotPosition.MIDFIELDER }
                    append("  - Midfielders: ")
                    appendln("[${midfielders.joinToString { player -> player.user!!.getDisplayName(match.channel?.guild) }}]")
                    append("```")
                }
            }

    fun printPlayPhase(): String =
        buildString {
            appendln(printPickBody(Phase.PLAY))
        }

    private fun printMatchTitle(phase: Phase): String =
        buildString {
            append("[${phase.title}] PUG Match ${type.title} (")
            append(
                when (phase) {
                    Phase.REGISTRATION -> getRegisteredUserCount()
                    Phase.PICKING, Phase.PLAY -> getPickedUserCount()
                }
            )
            append("/")
            append(getMaxRegisteredUserCount())
            append(')')
        }

    private fun printPickBody(phase: Phase = Phase.PICKING): String =
        buildString {
            appendln("```css")
            appendln(printMatchTitle(phase))
            appendln()
            appendln("Team 1:")
            appendln("  - Keeper: [${match.teamOne.getKeeper()?.user?.getDisplayName(match.channel?.guild) ?: ""}]")
            if (maxDefenders > 0)
                appendln(
                    "  - Defenders: [${match.teamOne.getDefender()?.user?.getDisplayName(match.channel?.guild) ?: ""}]"
                )
            appendln("  - Midfielders: [${match.teamOne.getMidfielders().joinToString { it.user!!.getDisplayName(match.channel?.guild) }}]")
            appendln()
            appendln("Team 2:")
            appendln("  - Keeper: [${match.teamTwo.getKeeper()?.user?.getDisplayName(match.channel?.guild) ?: ""}]")
            if (maxDefenders > 0)
                appendln(
                    "  - Defenders: [${match.teamTwo.getDefender()?.user?.getDisplayName(match.channel?.guild) ?: ""}]"
                )
            appendln("  - Midfielders: [${match.teamTwo.getMidfielders().joinToString { it.user!!.getDisplayName(match.channel?.guild) }}]")
            appendln()
            if (phase == Phase.PICKING)
                appendln("Current picker: [${pickingPlayer?.user?.getDisplayName(match.channel?.guild)}]")
            append("```")
        }

    private fun getRegisteredUserCount(): Int =
        keeperRegistrationQueue.size + defenderRegistrationQueue.size + midfielderRegistrationQueue.size

    private fun getMaxRegisteredUserCount(): Int = maxKeepers + maxDefenders + maxMidfielders

    private fun getPickedUserCount(): Int = arrayOf(match.teamOne, match.teamTwo)
        .fold(0) { acc, value ->
            acc + value.players.size
        }

    private fun checkFreeSlot(pickinPlayer: Player, pickedPlayer: Player): Boolean {
        val pickedPosition = pickedPlayer.position
        val pickerTeam = pickinPlayer.team ?: throw IllegalStateException("Picking player have no team")

        return when (pickedPosition!!) {
            SlotPosition.KEEPER -> pickerTeam.getKeeper()?.let { false } ?: true
            SlotPosition.DEFENDER -> pickerTeam.getDefender()?.let { false } ?: true
            SlotPosition.MIDFIELDER -> pickerTeam.getMidfielders().size < maxMidfielders / 2
        }
    }

    @EventSubscriber
    fun onPresenceUpdate(event: PresenceUpdateEvent) {
        if (phase === Phase.REGISTRATION && event.newPresence.status === StatusType.OFFLINE)
            if (match.registrationQueue.count { it.user!! == event.user } > 0) {
                remove(event.user, true).also {
                    it?.let { player ->
                        listener.onInactiveRemove(this, player)
                    }
                }
            }
    }
}
