package com.omega.module.pug.service.pug

import com.omega.discord.bot.service.MessageSender
import com.omega.module.pug.service.pug.`object`.Player
import com.omega.module.pug.service.pug.manager.PUGManager
import sx.blah.discord.handle.obj.IChannel


class PUGManagerListener(private val channel: IChannel) {

    fun onStart(manager: PUGManager) {
        MessageSender.sendMessage(
            channel,
            "**${manager.type.title} PUG match started**\n__`!h puga`__ to know how to add to the match"
        )
    }

    @Suppress("UNUSED_PARAMETER")
    fun onRegistrationAdd(manager: PUGManager, addedPlayer: Player) {
        MessageSender.sendMessage(channel, printRegistrationAddMessage(addedPlayer))
    }

    fun onRegistrationComplete(manager: PUGManager, lastAddedPlayer: Player) {
        buildString {
            appendln(printRegistrationAddMessage(lastAddedPlayer))
            appendln("**Registration phase now complete, pick your players**")
            appendln(manager.printPickingTurnNotice())
            appendln(manager.printPickablePlayers())
        }.also { MessageSender.sendMessage(channel, it) }
    }

    fun onRegistrationRemove(manager: PUGManager, player: Player) {
        MessageSender.sendMessage(
            channel,
            "**Removed __`${player.user?.getDisplayName(channel.guild)}`__ from the match\n${manager.printRegistrationPhase()}**"
        )
    }

    fun onPick(manager: PUGManager, pickingPlayer: Player, pickedPlayer: Player, nextPickingPlayer: Player) {
        MessageSender.sendMessage(
            channel,
            buildString {
                appendln(printPickedPlayerMessageLine(pickingPlayer, pickedPlayer))
                appendln("It's your turn to pick now ${nextPickingPlayer.user?.mention()} with command __`!pugp <userMention>`__")
                append(manager.printPickablePlayers())
            }
        )
    }

    fun onPickComplete(manager: PUGManager, lastPickingPlayer: Player, lastPickedPlayer: Player) {
        MessageSender.sendMessage(
            channel,
            buildString {
                appendln(printPickedPlayerMessageLine(lastPickingPlayer, lastPickedPlayer))
                appendln()
                append(manager.printPickPhase(true))
            }
        )
    }

    @Suppress("UNUSED_PARAMETER")
    fun onStop(manager: PUGManager) {
        MessageSender.sendMessage(channel, "**Current PUG match stopped**")
    }

    fun onError(message: String) {
        MessageSender.sendMessage(channel, message)
    }

    @Suppress("UNUSED_PARAMETER")
    fun onInactiveRemove(manager: PUGManager, player: Player) {
        MessageSender.sendMessage(channel, "Removed ${player.user?.getDisplayName(channel.guild)} from match for going offline")
    }

    private fun printPickedPlayerMessageLine(pickingPlayer: Player, pickedPlayer: Player) =
        "__`${pickingPlayer.user?.name}`__ picked __`${pickedPlayer.user?.getDisplayName(channel.guild)}`__"

    private fun printRegistrationAddMessage(addedPlayer: Player) =
            buildString {
                append("**Added __`")
                append(addedPlayer.user?.name)
                append("`__ to the match as __`")
                append(addedPlayer.position?.title)
                appendln("`__**")
                appendln(PUGService.printRegistration(channel))
            }
}