package com.omega.module.pug.service.pug.manager

import com.omega.module.pug.service.pug.PUGManagerListener
import com.omega.module.pug.service.pug.`object`.Slot
import com.omega.module.pug.service.pug.`object`.SlotPosition


class PUGManager2v2(listener: PUGManagerListener) : PUGManager(listener) {

    override val maxKeepers: Int = 2
    override val maxDefenders: Int = 0
    override val maxMidfielders: Int = 2
    override val type: Type = Type.T2v2

    init {
        arrayOf(match.teamOne, match.teamTwo).forEach {
            with(it.slots) {
                add(Slot(SlotPosition.KEEPER))
                add(Slot(SlotPosition.MIDFIELDER))
            }
        }
    }

    override fun isPositionAllowed(position: SlotPosition): Boolean =
        when (position) {
            SlotPosition.KEEPER, SlotPosition.MIDFIELDER -> true
            else -> false
        }
}