package com.omega.module.pug.service.pug.`object`

import org.mongodb.morphia.annotations.Embedded


@Embedded
class Team {

    @Transient
    val slots: MutableList<Slot> = mutableListOf()

    @Embedded
    val players: MutableList<Player> = mutableListOf()

    fun getKeeper() = players.find { it.position == SlotPosition.KEEPER }

    private fun getKeeperSlot(): Slot? = slots.find { it.position == SlotPosition.KEEPER }

    fun setKeeper(player: Player) {
        getKeeperSlot()
            ?.also {
                it.player?.let { oldPlayer -> players.remove(oldPlayer) }
                it.player = player
                player.team = this
                players.add(player)
            }
            ?: throw PUGException("Keeper slot already taken")
    }

    fun getDefender() = players.find { it.position == SlotPosition.DEFENDER }

    private fun getDefenderSlot(): Slot? = slots.find { it.position == SlotPosition.DEFENDER }

    fun setDefender(player: Player) {
        getDefenderSlot()
            ?.also {
                it.player?.let { oldPlayer -> players.remove(oldPlayer) }
                it.player = player
                player.team = this
                players.add(player)
            }
            ?: throw PUGException("Defender slot already taken")
    }

    fun getMidfielders(): List<Player> = players.filter { it.position == SlotPosition.MIDFIELDER }

    fun addMidfielder(player: Player) {
        getEmptyMidfielderSlot()
            ?.also {
                it.player?.let { oldPlayer -> players.remove(oldPlayer) }
                it.player = player
                player.team = this
                players.add(player)
            }
            ?: throw PUGException("All midfielder slots taken")
    }

    private fun getEmptyMidfielderSlot(): Slot? =
        slots.find { it.position == SlotPosition.MIDFIELDER && it.player == null }
}