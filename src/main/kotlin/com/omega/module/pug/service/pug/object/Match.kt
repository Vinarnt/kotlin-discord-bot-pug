package com.omega.module.pug.service.pug.`object`

import com.omega.module.pug.service.pug.manager.PUGManager
import org.bson.types.ObjectId
import org.mongodb.morphia.annotations.Embedded
import org.mongodb.morphia.annotations.Entity
import org.mongodb.morphia.annotations.Id
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.IGuild
import sx.blah.discord.handle.obj.IUser
import java.time.LocalDateTime


@Entity("matches", noClassnameStored = true)
data class Match(

    var channel: IChannel? = null,
    var guild: IGuild? = null,
    var type: PUGManager.Type? = null,
    var initiatedBy: IUser? = null,
    var initiatedAt: LocalDateTime? = null,
    var startedAt: LocalDateTime? = null,
    var stoppedBy: IUser? = null,
    var stoppedAt: LocalDateTime? = null,

    @Embedded
    val registrationQueue: MutableList<Player> = mutableListOf(),

    @Embedded
    val teamOne: Team = Team(),

    @Embedded
    val teamTwo: Team = Team(),

    @Id
    val id: ObjectId? = null
)