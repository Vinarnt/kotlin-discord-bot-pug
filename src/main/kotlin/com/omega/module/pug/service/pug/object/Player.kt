package com.omega.module.pug.service.pug.`object`

import org.mongodb.morphia.annotations.Embedded
import sx.blah.discord.handle.obj.IUser


@Embedded
data class Player constructor(

    @Embedded
    var user: IUser? = null,

    @Embedded
    var position: SlotPosition? = null,

    @Transient
    var team: Team? = null
)