package com.omega.module.pug.permission

import com.omega.discord.bot.permission.BasePermission


enum class Permission(override val key: String) : BasePermission {

    COMMAND_PUG_INFO("command.pug.info"),
    COMMAND_PUG_START("command.pug.start"),
    COMMAND_PUG_STOP("command.pug.stop"),
    COMMAND_PUG_ADD("command.pug.add"),
    COMMAND_PUG_ADD_FORCE("command.pug.add.force"),
    COMMAND_PUG_REMOVE("command.pug.remove"),
    COMMAND_PUG_REMOVE_FORCE("command.pug.remove.force"),
    COMMAND_PUG_PICK("command.pug.pick"),
    COMMAND_PUG_LOG("command.pug.log"),
    COMMAND_PUG_LOG_PLAYED_BY("command.pug.log.playedby")
}