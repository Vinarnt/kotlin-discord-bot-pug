package com.omega.module.pug.command.impl

import com.omega.discord.bot.command.Command
import com.omega.discord.bot.permission.BasePermission
import com.omega.discord.bot.service.MessageSender
import com.omega.module.pug.permission.Permission
import com.omega.module.pug.service.pug.PUGService
import com.omega.module.pug.service.pug.`object`.PUGException
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.IMessage
import sx.blah.discord.handle.obj.IUser


class PugStopCommand : Command() {
    override val name: String = "pugstop"
    override val aliases: Array<String>? = arrayOf("pugst")
    override val usage: String = "**pugstop** - Stop the PUG match of the channel"
    override val allowPrivate: Boolean = false
    override val permission: BasePermission? = Permission.COMMAND_PUG_STOP
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 2

    override fun execute(author: IUser, channel: IChannel, message: IMessage, args: List<String>) {
        try {
            PUGService.stop(author, channel)
        } catch (e: PUGException) {
            MessageSender.sendMessage(channel, e.message!!)
        }
    }
}