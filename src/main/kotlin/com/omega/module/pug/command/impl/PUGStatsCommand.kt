package com.omega.module.pug.command.impl

import com.omega.discord.bot.command.Command
import com.omega.discord.bot.permission.BasePermission
import com.omega.discord.bot.service.MessageSender
import com.omega.module.pug.service.pug.PUGMatchLogger
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.IMessage
import sx.blah.discord.handle.obj.IUser
import sx.blah.discord.util.EmbedBuilder
import java.awt.Color


class PUGStatsCommand : Command() {
    override val name: String = "pugstats"
    override val aliases: Array<String>? = null
    override val usage: String = "**pugstats** - Get global stats on the pugs"
    override val allowPrivate: Boolean = false
    override val permission: BasePermission? = null
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(author: IUser, channel: IChannel, message: IMessage, args: List<String>) {
        when {
            args.isEmpty() -> globalStats(channel)
        }
    }

    private fun globalStats(channel: IChannel) {
        val initiateMatches = PUGMatchLogger.initiatedMatchesCount(channel.guild)
        val startedMatches = PUGMatchLogger.startedMatchesCount(channel.guild)
        val stoppedMatches = PUGMatchLogger.stoppedMatchesCount(channel.guild)

        EmbedBuilder()
            .withTitle("Global PUG stats")
            .withColor(Color.GREEN)
            .appendField("Matches", buildString {
                append("Initiated: ").appendln(initiateMatches)
                append("Started: ").appendln(startedMatches)
                append("Stopped: ").appendln(stoppedMatches)
            }, true)
            .also {
                MessageSender.sendMessage(channel, it)
            }
    }
}