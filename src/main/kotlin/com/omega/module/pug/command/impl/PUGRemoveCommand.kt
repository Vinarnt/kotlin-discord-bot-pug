package com.omega.module.pug.command.impl

import com.omega.discord.bot.command.Command
import com.omega.discord.bot.ext.StringUtils
import com.omega.discord.bot.permission.BasePermission
import com.omega.discord.bot.permission.PermissionManager
import com.omega.discord.bot.service.MessageSender
import com.omega.module.pug.permission.Permission
import com.omega.module.pug.service.pug.PUGService
import com.omega.module.pug.service.pug.`object`.PUGException
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.IMessage
import sx.blah.discord.handle.obj.IUser


class PUGRemoveCommand : Command() {
    override val name: String = "pugremove"
    override val aliases: Array<String>? = arrayOf("pugr")
    override val usage: String = "**pugremove** - Remove yourself from the current channel pug match\n" +
            "**pugremove <userMention>** - Remove the mentioned user from the current channel pug match"
    override val allowPrivate: Boolean = false
    override val permission: BasePermission? = Permission.COMMAND_PUG_REMOVE
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override
    fun execute(author: IUser, channel: IChannel, message: IMessage, args: List<String>) {

        when {
            // Remove the author
            args.isEmpty() -> {
                remove(channel, author)
            }

            // Remove the mentioned user
            StringUtils.isUserMention(args.first()) -> {
                // Check permission
                if (!PermissionManager.hasPermission(channel.guild, author, Permission.COMMAND_PUG_REMOVE_FORCE))
                    return missingPermission(author, message, Permission.COMMAND_PUG_REMOVE_FORCE)

                StringUtils.parseUserMention(args.first())?.let {
                    remove(channel, it)
                } ?: MessageSender.sendMessage(channel, "Unable to get user from mention")
            }

            // Provided parameter is not a user mention
            else -> MessageSender.sendMessage(channel, "User mention expected as first argument")
        }
    }

    private fun remove(channel: IChannel, target: IUser) {
        try {
            PUGService.remove(channel, target)
        } catch (e: PUGException) {
            MessageSender.sendMessage(channel, e.message!!)
        }
    }
}