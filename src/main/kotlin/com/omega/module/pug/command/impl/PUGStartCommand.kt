package com.omega.module.pug.command.impl

import com.omega.discord.bot.command.Command
import com.omega.discord.bot.permission.BasePermission
import com.omega.discord.bot.service.MessageSender
import com.omega.module.pug.permission.Permission
import com.omega.module.pug.service.pug.PUGService
import com.omega.module.pug.service.pug.`object`.PUGException
import com.omega.module.pug.service.pug.manager.PUGManager
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.IMessage
import sx.blah.discord.handle.obj.IUser


class PUGStartCommand : Command() {

    override val name: String = "pugstart"
    override val aliases: Array<String>? = arrayOf("pugs")
    override val usage: String = "**pugstart <3v3|5v5|6v6>** - Start a pug match for 3v3, 5v5 or 6v6"
    override val allowPrivate: Boolean = false
    override val permission: BasePermission? = Permission.COMMAND_PUG_START
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(author: IUser, channel: IChannel, message: IMessage, args: List<String>) {
        when {
            // Missing arguments
            args.isEmpty() -> missingArgs(author, message)

            // Argument is correct
            args.first().matches(Regex("^\\s*(?:2v2|3v3|5v5|6v6)\\s*\$", RegexOption.IGNORE_CASE)) -> {

                // Check
                val matchType: PUGManager.Type = when {
                    args.first().equals("2v2", true) -> PUGManager.Type.T2v2
                    args.first().equals("3v3", true) -> PUGManager.Type.T3V3
                    args.first().equals("5v5", true) -> PUGManager.Type.T5V5
                    else -> PUGManager.Type.T6V6
                }

                // Start match
                try {
                    PUGService.start(author, channel, matchType)
                } catch (e: PUGException) {
                    MessageSender.sendMessage(channel, e.message!!)
                }
            }

            // Wrong arguments provided
            else -> {
                MessageSender.sendMessage(
                    channel,
                    "Wrong mode provided.\n\n\t**Supported modes are:** 3v3, 5v5 and 6v6"
                )
            }
        }
    }
}