package com.omega.module.pug.command.impl

import com.omega.discord.bot.command.Command
import com.omega.discord.bot.ext.StringUtils
import com.omega.discord.bot.permission.BasePermission
import com.omega.discord.bot.service.MessageSender
import com.omega.module.pug.permission.Permission
import com.omega.module.pug.service.pug.PUGService
import com.omega.module.pug.service.pug.`object`.PUGException
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.IMessage
import sx.blah.discord.handle.obj.IUser


class PUGPickCommand : Command() {

    override val name: String = "pubpick"
    override val aliases: Array<String>? = arrayOf("pugp")
    override val usage: String = "pugpick <userMention>"
    override val allowPrivate: Boolean = false
    override val permission: BasePermission? = Permission.COMMAND_PUG_PICK
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(author: IUser, channel: IChannel, message: IMessage, args: List<String>) {
        when {
            // Missing arguments
            args.isEmpty() -> missingArgs(author, message)

            // User mention provided
            StringUtils.isUserMention(args.first()) -> {

                StringUtils.parseUserMention(args.first())?.let {
                    try {
                        PUGService.pick(channel, author, it)
                    } catch (e: PUGException) {
                        MessageSender.sendMessage(channel, e.message!!)
                    }
                } ?: MessageSender.sendMessage(
                    channel, "Unable to get mentioned user"
                )
            }

            // No user mention provided
            else -> MessageSender.sendMessage(channel, "Only user mentions are allowed")
        }
    }
}