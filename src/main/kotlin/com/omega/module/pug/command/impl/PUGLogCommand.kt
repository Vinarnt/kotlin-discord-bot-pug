package com.omega.module.pug.command.impl

import com.omega.discord.bot.command.Command
import com.omega.discord.bot.ext.StringUtils
import com.omega.discord.bot.permission.BasePermission
import com.omega.discord.bot.permission.PermissionManager
import com.omega.discord.bot.service.MessageSender
import com.omega.module.pug.permission.Permission
import com.omega.module.pug.service.pug.PUGMatchLogger
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.IMessage
import sx.blah.discord.handle.obj.IUser
import java.io.*


class PUGLogCommand : Command() {
    override val name: String = "puglog"
    override val aliases: Array<String>? = arrayOf("pugl")
    override val usage: String = "**puglog <userMention>** - Get logs of matches played by the provided user"
    override val allowPrivate: Boolean = false
    override val permission: BasePermission? = null
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 5

    override fun execute(author: IUser, channel: IChannel, message: IMessage, args: List<String>) {

        when {
            args.isEmpty() -> missingArgs(author, message)

            // Get matches played by the user
            StringUtils.isUserMention(args.first()) -> {
                if (PermissionManager.hasPermission(channel.guild, author, Permission.COMMAND_PUG_LOG_PLAYED_BY)) {
                    StringUtils.parseUserMention(args.first())?.also { user ->
                        PUGMatchLogger.getMatchesPlayedBy(channel.guild, user).let { matches ->
                            MessageSender.sendMessage(
                                channel, "Matches played by ${user.name}", "${user.name}_played_matches.txt",

                                // TODO: Extract this crap and rework it
                                ByteArrayOutputStream().use { baout ->
                                    BufferedOutputStream(baout).use { bout ->
                                        PrintWriter(bout).use { writter ->
                                            matches.forEach { match ->
                                                writter.apply {
                                                    println("PUG match ${match.type} from ${match.initiatedAt} by ${match.initiatedBy?.name}")
                                                    println("Team One")
                                                    println(
                                                        match.teamOne.players.joinToString(", ") { player ->
                                                            "${player.user!!.name}: ${player.position}"
                                                        }
                                                    )
                                                    println()
                                                    println("Team Two")
                                                    println(
                                                        match.teamTwo.players.joinToString(", ") { player ->
                                                            "${player.user!!.name}: ${player.position}"
                                                        }
                                                    )
                                                    match.startedAt?.let { println("Started at ${match.startedAt}") }
                                                    match.stoppedAt?.let { println("Stopped at ${match.stoppedAt} by ${match.stoppedBy?.name}") }
                                                }
                                            }
                                            writter.flush()
                                        }
                                        baout
                                    }.let {
                                        BufferedInputStream(ByteArrayInputStream(it.toByteArray()))
                                    }
                                }
                            )
                        }
                    } ?: MessageSender.sendMessage(channel, "User mention expected as first argument")
                } else
                    missingPermission(author, message, Permission.COMMAND_PUG_LOG_PLAYED_BY)
            }
        }
    }
}