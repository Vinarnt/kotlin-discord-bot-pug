package com.omega.module.pug.command.impl

import com.omega.discord.bot.command.Command
import com.omega.discord.bot.ext.StringUtils
import com.omega.discord.bot.permission.BasePermission
import com.omega.discord.bot.permission.PermissionManager
import com.omega.discord.bot.service.MessageSender
import com.omega.module.pug.permission.Permission
import com.omega.module.pug.service.pug.PUGService
import com.omega.module.pug.service.pug.`object`.PUGException
import com.omega.module.pug.service.pug.`object`.SlotPosition
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.IMessage
import sx.blah.discord.handle.obj.IUser


class PUGAddCommand : Command() {

    override val name: String = "pugadd"
    override val aliases: Array<String>? = arrayOf("puga")
    override val usage: String = "**pugadd <keep|k|def|d|mid|m>** - Add yourself to the match at the given position\n" +
            "**pugadd <userMention> <keep|k|def|d||mid|m|>** - Add the mentioned user to the match at the given position\n\n" +
            "**Positions**\n" +
            " - `keep` or `k` for Keeper position\n" +
            " - `def` or `d` for Defender position\n" +
            " - `mid` or `m` for Midfielder position"
    override val allowPrivate: Boolean = false
    override val permission: BasePermission? = Permission.COMMAND_PUG_ADD
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(author: IUser, channel: IChannel, message: IMessage, args: List<String>) {
        // Get the first argument
        val firstArg: String? = args.firstOrNull()

        when {
            // Missing argument
            firstArg == null -> missingArgs(author, message)

            // Self add
            args.size < 2 -> {
                add(channel, author, firstArg)
            }
            // Force add
            else -> {
                if (StringUtils.isUserMention(firstArg)) {
                    // Check permission
                    if (!PermissionManager.hasPermission(channel.guild, author, Permission.COMMAND_PUG_ADD_FORCE))
                        return missingPermission(author, message, Permission.COMMAND_PUG_ADD_FORCE)

                    StringUtils.parseUserMention(firstArg)?.let {

                        // Check for second arg
                        val targetPosition: String = args[1]

                        add(channel, it, targetPosition)
                    } ?: MessageSender.sendMessage(channel, "Unable to get user from mention")

                } else
                    MessageSender.sendMessage(channel, "User mention expected as first argument")
            }
        }
    }

    private fun add(channel: IChannel, target: IUser, positionArg: String) {
        // Provided position is correct
        if (positionArg.matches(Regex("^(?:keep|def|mid|[kdm])$", RegexOption.IGNORE_CASE))) {
            val position: SlotPosition? = when {
                positionArg.equals("keep", true) || positionArg.equals("k", true) -> SlotPosition.KEEPER
                positionArg.equals("def", true) || positionArg.equals("d", true) -> SlotPosition.DEFENDER
                positionArg.equals("mid", true) || positionArg.equals("m", true) -> SlotPosition.MIDFIELDER
                else -> null
            }

            // Add to registration queue
            try {
                PUGService.add(channel, target, position!!)
            } catch (e: PUGException) {
                MessageSender.sendMessage(channel, e.message!!)
            }
        } else {
            MessageSender.sendMessage(channel, "**Wrong position provided.**\n\n\t$usage")
        }
    }
}