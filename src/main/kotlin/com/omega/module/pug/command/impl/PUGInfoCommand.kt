package com.omega.module.pug.command.impl

import com.omega.discord.bot.command.Command
import com.omega.discord.bot.permission.BasePermission
import com.omega.discord.bot.service.MessageSender
import com.omega.module.pug.permission.Permission
import com.omega.module.pug.service.pug.PUGService
import com.omega.module.pug.service.pug.`object`.PUGException
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.IMessage
import sx.blah.discord.handle.obj.IUser


class PUGInfoCommand : Command() {

    override val name: String = "puginfo"
    override val aliases: Array<String>? = arrayOf("pugi")
    override val usage: String = "**puginfo** - Get info on the current channel PUG match"
    override val allowPrivate: Boolean = false
    override val permission: BasePermission? = Permission.COMMAND_PUG_INFO
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 2

    override fun execute(author: IUser, channel: IChannel, message: IMessage, args: List<String>) {
        try {
            MessageSender.sendMessage(channel, PUGService.printInfo(channel))
        } catch (e: PUGException) {
            MessageSender.sendMessage(channel, e.message!!)
        }
    }
}